
def buildImage() {
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repository', passwordVariable: 'PASS',usernameVariable: 'USER')]) {
    sh 'docker build -t elyor98/demo-app:1.0 .'
    sh "echo $PASS | docker login -u $USER --password-stdin"
    sh 'docker push elyor98/demo-app:1.0'
    }
}

def deploy() {
    def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"
    def ubuntu = "ubuntu@18.193.115.156"
    sshagent(['ubuntu']) {
        sh "scp docker-compose.yaml ${ubuntu}:/home/ubuntu"
        sh "ssh -o StrictHostKeyChecking=no ${ubuntu} ${dockerComposeCmd}"
    }
}



return this